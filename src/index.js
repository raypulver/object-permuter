"use strict";

module.exports = o;

const { keys }  = Object;

function o(obj) {
  return keys(obj).map((v) => {
    return [v, helper(obj[v])];
  });
}

function helper(obj) {
  if (typeof obj === 'object') return keys(obj).reduce((r, v) => {
    return r.concat(helper(obj[v]).map((i) => {
      i.unshift(v);
      return i;
    }));
  }, []);
  return [[obj]];
}
