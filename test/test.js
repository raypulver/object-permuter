var obj = {
  key: {
    secondkey: {
      thirdkey: 5,
      fourthkey: 6,
      fifthkey: {
        sixthkey: 7,
        seventhkey: 8
      }
    },
    eighthkey: 7
  },
  doop: 6
};


var permuter = require('..');
var expect = require('chai').expect;

describe('object permuter', function () {
  it('should permute an object', function () {
    expect(permuter(obj)).to.eql([
      ['key', [
        ['secondkey', 'thirdkey', 5],
        ['secondkey', 'fourthkey', 6],
        ['secondkey', 'fifthkey', 'sixthkey', 7],
        ['secondkey', 'fifthkey', 'seventhkey', 8],
        ['eighthkey', 7]
      ]],
      ['doop', [[6]]]
    ]);
  });
});
