"use strict";

const gulp = require('gulp'),
  path = require('path'),
  fs = require('fs'),
  _ = require('lodash'),
  gutil = require('gulp-util'),
  runSequence = require('run-sequence'),
  mocha = require('gulp-mocha'),
  babel = require('gulp-babel'),
  jshint = require('gulp-jshint'),
  spawnSync = require('child_process').spawnSync,
  clone = require('clone'),
  map = require('async').map;

let jshintConfig = {};

require.extensions['.json'](jshintConfig, './.jshintrc');

let jshintServerConfig = jshintConfig.exports;
let jshintClientConfig = clone(jshintServerConfig);

jshintClientConfig.predef = jshintClientConfig.predef.client;
jshintServerConfig.predef = jshintServerConfig.predef.server;

let packageJson = require('./package');

gulp.task('default', ['build-and-test']);

const reserved = ['default'];

const srcPath = 'src/**/*.js';

jshint.client = jshint.bind(null, jshintClientConfig);
jshint.server = jshint.bind(null, jshintServerConfig);

function buildExtTask(testFramework, executable) {
  let task = `node ${path.join('node_modules', testFramework, executable)}`;
  if (arguments.length > 2) {
    task += ' ';
    task += [].slice.call(arguments, 2).join(' ');
  }
  return task;
}

gulp.task('build:tasks', function () {
  packageJson.scripts = {};
  _.forOwn(gulp.tasks, function (value, key, obj) {
    if (~reserved.indexOf(key)) return;
    packageJson.scripts[key] = `node ${path.join('node_modules', 'gulp', 'bin', 'gulp')} ${key}`;
  });
  fs.writeFileSync('./package.json', JSON.stringify(packageJson, null, 1));
});

gulp.task('build-and-test', function (cb) {
  runSequence(['build', 'test'], cb);
});

gulp.task('watch', ['default'], function () {
  return gulp.watch(srcPath, ['default']);
});

gulp.task('build', ['build:app']);

gulp.task('build:app', function () {
  gulp.src('src/**/*.js')
    .pipe(babel({ presets: ['es2015'] }))
    .pipe(gulp.dest('dist'));
});

gulp.task('jshint', ['jshint:app']);


gulp.task('jshint:app', function () {
  return gulp.src('./src/**/*.js')
    .pipe(jshint.server())
    .pipe(jshint.reporter('jshint-stylish'));
});
