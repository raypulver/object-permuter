"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

module.exports = o;

var keys = Object.keys;


function o(obj) {
  return keys(obj).map(function (v) {
    return [v, helper(obj[v])];
  });
}

function helper(obj) {
  if ((typeof obj === "undefined" ? "undefined" : _typeof(obj)) === 'object') return keys(obj).reduce(function (r, v) {
    return r.concat(helper(obj[v]).map(function (i) {
      i.unshift(v);
      return i;
    }));
  }, []);
  return [[obj]];
}